// Juan Carlos Lopez Briseño
// Juan Carlos Verdugo Salazar

// El proyecto se empezó con una idea donde todo fuera general y reutilizable, con forme fuimos programando seguimos estas reglas
// para escribir el menor codigo posible, y no superar las 500 lineas de codigo, llenando la logica con puros recorridos de "FOR".
// Nosotros quizimos hacer un metodo de identificación de patrones utilizando expresiones regulares, los cuales investigamos a fondo por un buen tiempo
// para poder utilizarlas. Estas expresiones son de mucha ayuda al validar textos buscando patrones (Coincidencias).
// Como hay cartas con el mismo patron nuestras ideas era identificarlos a través de las expressiones, que vienen en info.js, dentro
// De arreglos de cada tipo de carta. Conforme fuimos avanzando se nos fue dificultando.
// Cosas que puede hacer:
// 1. Enviar cartas al azar con un modal de visualización y una descripción.
// 2. Generar recursos al azar, y el recurso seleccionado cambiar al azar
// 3. Identificar si se a formado un patron (18 cartas)
// 4. Guardar todos los indices de los patrones de las cartas verdes en un arreglo general y no permitir que se repitan.
// El metodo de buscar indices no se ejecuta hasta que haya una coincidencia con las expresiones regulares, evitando tener que recorrer con ciclos cada
// vez que sea puesto un recurso.
// 5. Utilizar el maestro constructo para construir las cartas verdes.
// 6. Una funcion general para todas las cartas verdes, y el patron es traido a traves del arreglo con la carta que toco al azar, de esta manera se pueden validar
// todas esas cartas con una sola funcion, esa era nuestra idea principal, todas las cartass tuvieras una unica función.

var tableGame = [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '];
changeResources();
changeImage();

function changeImage() {
	var random = Math.floor(Math.random() * blues.length);
	document.getElementById("bluesDisplay").src = blues[random].name;
	document.getElementById("bluesDisplay").classList.add(random);

	random = Math.floor(Math.random() * oranges.length);
	document.getElementById("orangesDisplay").src = oranges[random].name;
	document.getElementById("orangesDisplay").classList.add(random);

	random = Math.floor(Math.random() * reds.length);
	document.getElementById("redsDisplay").src = reds[random].name;
	document.getElementById("redsDisplay").classList.add(random);

	random = Math.floor(Math.random() * greens.length);
	document.getElementById("greensDisplay").src = greens[random].name;
	document.getElementById("greensDisplay").classList.add(random);

	random = Math.floor(Math.random() * grays.length);
	document.getElementById("graysDisplay").src = grays[random].name;
	document.getElementById("graysDisplay").classList.add(random);

	random = Math.floor(Math.random() * yellows.length);
	document.getElementById("yellowsDisplay").src = yellows[random].name;
	document.getElementById("yellowsDisplay").classList.add(random);

	random = Math.floor(Math.random() * blacks.length);
	document.getElementById("blacksDisplay").src = blacks[random].name;
	document.getElementById("blacksDisplay").classList.add(random);

	random = Math.floor(Math.random() * pinks.length);
	document.getElementById("pinksDisplay").src = pinks[random].name;
	document.getElementById("pinksDisplay").classList.add(random);
}

function changeResources(indexToChange) {
	for (let index = 1; index <= 3; index++) {
		let button;
		let randomResource = Math.floor(Math.random() * (resources.length));
		switch (randomResource) {
			case 0:
				button = "<button id='wood' class='button' style='background-color: #7A3734; border: 1px solid black; margin: 10px; padding: 5px; color: #fff;'>  Madera </button>";
				break;
			case 1:
				button = "<button id='wheat' class='button' style='background-color: #FCAE1F; color: black; border: 1px solid black;margin: 10px;padding: 5px;'>  Trigo </button>";
				break;
			case 2:
				button = "<button id='brick'class='button' style='background-color: #F73324; border: 1px solid black;margin: 10px;padding: 5px;color: #fff;'>  Ladrillo </button>";
				break;
			case 3:
				button = "<button id='glass' class='button' style='background-color: #00949E; border: 1px solid black;margin: 10px;padding: 5px;color: #fff;'>  Vidrio </button>";
				break;
			case 4:
				button = "<button id='stone' class='button' style='background-color: #A59E87; border: 1px solid black;margin: 10px;padding: 5px;'>  Piedra </button>";
				break;
		}
		if (indexToChange) {
			document.getElementById(indexToChange).innerHTML = button;
			return ;
		}
		document.getElementById('resourceDisplay'+index).innerHTML = button;
	}
}


function watchImage(id, clase) {
	let src = document.getElementById(id).getAttribute('src');
	document.getElementById('image-modal').src = src;
	let position = document.getElementById(id).className;
	let type;
	switch (clase) {
		case 'blues': type = blues; break; case 'reds': type = reds; break; case 'grays': type = grays; break; case 'pinks': type = pinks; break;
		case 'oranges': type = oranges; break; case 'greens': type = greens; break; case 'yellows': type = yellows; break; case 'blacks': type = blacks; break;
	}
	document.getElementById('description').innerHTML = type[position].description;
}

function check() {
	if (window.location.hash == '#miModal') {
		window.location = './index.html';
	}
}

var resource = null;
function chooseResource(id) {
	if (resource == null) {
		let button;
		let x = document.getElementById(id).innerHTML;
		x = x.split(' ');
		x = x[1].split('"');
		resource = x[1];
		console.log(resource);
		changeResources(id);
	} else {
		alert('Ya tienes un recurso seleccionado.');
	}
}

function putResource(idCard) {
	let card = document.getElementById(idCard);
	if (resource) {
		if (!card.classList.contains('disabled')) {
			card.src = './img/' + resource + '.png'
			card.classList.add('disabled');
			tableGame[idCard] = resource.charAt(0).toUpperCase();
			if (resource == 'wheat') {
				tableGame[idCard] = 'T';
			}
			resource = null;
			checkTheTableGame();
		} else {
			alert('Ya hay un recurso aqui.');
		}
	} else {
		alert('Selecciona un recurso.');
	}
}

let search;
function checkTheTableGame() {
	let newTableGame = '-';
	for (let x = 0; x < tableGame.length; x++) {
		newTableGame += tableGame[x];
		if (x == 3 || x == 7 || x == 11 || x == 15) {
			newTableGame += '-';
		}
	}
	let grenObject = greens[document.getElementById('greensDisplay').className];
	search = grenObject.key.exec(newTableGame);
	console.log(search);
	if (search) {
		checkGreens(search, grenObject.pattern);
	}
}
var bandera = false;
function foundIt(positions, card) {
	if (!bandera) {
		alert('Ya puedes construir, presiona el boton de Maestro constructor y selecciona las cordenadas debajo del boton.');
		bandera = true;
	}
	for (let index = 0; index < 3; index++) {
		document.getElementById(`box_${positions[index]}`).classList.add('found');
	}
	buildings[buildCount] = JSON.stringify(positions);
	buildCount++;
	console.log(buildings);
	putTheBackground(card, positions);
}

var buildings = [];
var buildCount = 0;
function checkGreens(exec, pattern) {
	let TG = tableGame;
	let length = exec[0].length;
	let positions;
	if (length == 6) {
		// Horizontal
		for (let i = 0; i <= TG.length; i += 4) {
			if (TG[i]+TG[i+1]+TG[i+2] == pattern | TG[i]+TG[i+1]+TG[i+2] == pattern.split('').reverse().join('')) {
				positions = [i, i+1, i+2];
				searchingPosition(positions, 'green')
			}else if(TG[i+1]+TG[i+2]+TG[i+3] == pattern| TG[i+1]+TG[i+2]+TG[i+3] == pattern.split('').reverse().join('')){
				positions = [i+1, i+2, i+3]
				searchingPosition(positions, 'green')
			}
		}
	}else {
		// Vertical
		for (let i = 0; i < 4; i++) {
			if (TG[i]+TG[i+4]+TG[i+8] == pattern| TG[i]+TG[i+4]+TG[i+8] == pattern.split('').reverse().join('')) {
				positions = [i, i+4, i+8];
				searchingPosition(positions, 'green')
			}else if(TG[i+4]+TG[i+8]+TG[i+12] == pattern| TG[i+4]+TG[i+8]+TG[i+12] == pattern.split('').reverse().join('')){
				positions = [i+4, i+8, i+12];
				searchingPosition(positions, 'green')
			}
		}
	}
}

var positionBuild;
function searchingPosition(positions, color) {
	positionBuild = positions
	if(JSON.stringify(buildings).includes(JSON.stringify(positions))){
		 console.log('Ya esta');
	}else{
		foundIt(positions, color);
   }
}

function putTheBackground(color, position) {
	let cards = ''
	let idA = 0;;
	for (const iterator of position) {
		cards += '-'+(iterator + 1);
	}
	cards += '-'
	document.getElementById('Construcciones').innerHTML += '<br> <div id="idA"><a style="style="cursor: pointer;" onclick="built()">Carta de color: '+ color +', con las casillas '+ JSON.stringify(cards)+ '</a><div><br>';
	idA++;
}

var canB = false;
 function canBuild(){
	canB = true
	setTimeout(() => {
		canB = false
	}, 10000);
}

function built() {
	if (canB) {
		let response = prompt('Selecciona la casilla a construir');
		document.getElementById((response - 1).toString()).src = 'img/greenHouse.png';
		document.getElementById('idA').innerHTML = '';
		for (const iterator of positionBuild) {
			if (iterator != (response-1)) {
				document.getElementById('box_'+(iterator).toString()).classList.remove('found');
				document.getElementById((iterator).toString()).classList.remove('disabled');
				document.getElementById((iterator).toString()).src = '';
				tableGame[iterator] = ' ';
			}
			tableGame[response-1] = '.'
		}

	}
}